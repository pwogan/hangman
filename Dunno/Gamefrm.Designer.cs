﻿namespace Dunno
{
    partial class Gamefrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelWord = new System.Windows.Forms.Label();
            this.textBoxWord = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panelArmL = new System.Windows.Forms.Panel();
            this.panelArmR = new System.Windows.Forms.Panel();
            this.panelLegR = new System.Windows.Forms.Panel();
            this.panelLegL = new System.Windows.Forms.Panel();
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelHead = new System.Windows.Forms.Panel();
            this.btnGuess = new System.Windows.Forms.Button();
            this.labelGuess = new System.Windows.Forms.Label();
            this.textBoxGuess = new System.Windows.Forms.TextBox();
            this.textBoxGuessWord = new System.Windows.Forms.TextBox();
            this.textBoxGenre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelGenre = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxIncorrect = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelWord
            // 
            this.labelWord.AutoSize = true;
            this.labelWord.Location = new System.Drawing.Point(32, 17);
            this.labelWord.Name = "labelWord";
            this.labelWord.Size = new System.Drawing.Size(191, 13);
            this.labelWord.TabIndex = 0;
            this.labelWord.Text = "Please enter the word(s) to be guessed";
            this.labelWord.Click += new System.EventHandler(this.labelWord_Click);
            // 
            // textBoxWord
            // 
            this.textBoxWord.Location = new System.Drawing.Point(35, 33);
            this.textBoxWord.Name = "textBoxWord";
            this.textBoxWord.PasswordChar = '*';
            this.textBoxWord.Size = new System.Drawing.Size(177, 20);
            this.textBoxWord.TabIndex = 1;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(84, 97);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 3;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxGenre);
            this.panel1.Controls.Add(this.labelWord);
            this.panel1.Controls.Add(this.buttonOK);
            this.panel1.Controls.Add(this.textBoxWord);
            this.panel1.Location = new System.Drawing.Point(83, 90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(254, 123);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.textBoxIncorrect);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.labelGenre);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panelArmL);
            this.panel2.Controls.Add(this.panelArmR);
            this.panel2.Controls.Add(this.panelLegR);
            this.panel2.Controls.Add(this.panelLegL);
            this.panel2.Controls.Add(this.panelBody);
            this.panel2.Controls.Add(this.panelHead);
            this.panel2.Controls.Add(this.btnGuess);
            this.panel2.Controls.Add(this.labelGuess);
            this.panel2.Controls.Add(this.textBoxGuess);
            this.panel2.Controls.Add(this.textBoxGuessWord);
            this.panel2.Location = new System.Drawing.Point(27, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(361, 272);
            this.panel2.TabIndex = 4;
            this.panel2.Visible = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Black;
            this.panel9.Location = new System.Drawing.Point(191, 56);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(103, 10);
            this.panel9.TabIndex = 11;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Black;
            this.panel8.Location = new System.Drawing.Point(191, 62);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(11, 140);
            this.panel8.TabIndex = 10;
            // 
            // panelArmL
            // 
            this.panelArmL.BackColor = System.Drawing.Color.Black;
            this.panelArmL.Location = new System.Drawing.Point(242, 106);
            this.panelArmL.Name = "panelArmL";
            this.panelArmL.Size = new System.Drawing.Size(13, 28);
            this.panelArmL.TabIndex = 9;
            this.panelArmL.Visible = false;
            // 
            // panelArmR
            // 
            this.panelArmR.BackColor = System.Drawing.Color.Black;
            this.panelArmR.Location = new System.Drawing.Point(292, 106);
            this.panelArmR.Name = "panelArmR";
            this.panelArmR.Size = new System.Drawing.Size(13, 28);
            this.panelArmR.TabIndex = 8;
            this.panelArmR.Visible = false;
            // 
            // panelLegR
            // 
            this.panelLegR.BackColor = System.Drawing.Color.Black;
            this.panelLegR.Location = new System.Drawing.Point(276, 158);
            this.panelLegR.Name = "panelLegR";
            this.panelLegR.Size = new System.Drawing.Size(10, 28);
            this.panelLegR.TabIndex = 7;
            this.panelLegR.Visible = false;
            // 
            // panelLegL
            // 
            this.panelLegL.BackColor = System.Drawing.Color.Black;
            this.panelLegL.Location = new System.Drawing.Point(261, 158);
            this.panelLegL.Name = "panelLegL";
            this.panelLegL.Size = new System.Drawing.Size(10, 28);
            this.panelLegL.TabIndex = 6;
            this.panelLegL.Visible = false;
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.Color.Black;
            this.panelBody.Location = new System.Drawing.Point(261, 106);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(25, 46);
            this.panelBody.TabIndex = 5;
            this.panelBody.Visible = false;
            // 
            // panelHead
            // 
            this.panelHead.BackColor = System.Drawing.Color.Black;
            this.panelHead.Location = new System.Drawing.Point(261, 72);
            this.panelHead.Name = "panelHead";
            this.panelHead.Size = new System.Drawing.Size(25, 28);
            this.panelHead.TabIndex = 4;
            this.panelHead.Visible = false;
            // 
            // btnGuess
            // 
            this.btnGuess.Location = new System.Drawing.Point(22, 108);
            this.btnGuess.Name = "btnGuess";
            this.btnGuess.Size = new System.Drawing.Size(75, 23);
            this.btnGuess.TabIndex = 2;
            this.btnGuess.Text = "Guess";
            this.btnGuess.UseVisualStyleBackColor = true;
            this.btnGuess.Click += new System.EventHandler(this.btnGuess_Click);
            // 
            // labelGuess
            // 
            this.labelGuess.AutoSize = true;
            this.labelGuess.Location = new System.Drawing.Point(22, 43);
            this.labelGuess.Name = "labelGuess";
            this.labelGuess.Size = new System.Drawing.Size(72, 13);
            this.labelGuess.TabIndex = 3;
            this.labelGuess.Text = "Guess a letter";
            // 
            // textBoxGuess
            // 
            this.textBoxGuess.Location = new System.Drawing.Point(22, 72);
            this.textBoxGuess.Name = "textBoxGuess";
            this.textBoxGuess.Size = new System.Drawing.Size(78, 20);
            this.textBoxGuess.TabIndex = 1;
            // 
            // textBoxGuessWord
            // 
            this.textBoxGuessWord.Location = new System.Drawing.Point(140, 228);
            this.textBoxGuessWord.Name = "textBoxGuessWord";
            this.textBoxGuessWord.ReadOnly = true;
            this.textBoxGuessWord.Size = new System.Drawing.Size(193, 20);
            this.textBoxGuessWord.TabIndex = 0;
            // 
            // textBoxGenre
            // 
            this.textBoxGenre.Location = new System.Drawing.Point(35, 71);
            this.textBoxGenre.Name = "textBoxGenre";
            this.textBoxGenre.Size = new System.Drawing.Size(177, 20);
            this.textBoxGenre.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Genre";
            // 
            // labelGenre
            // 
            this.labelGenre.AutoSize = true;
            this.labelGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGenre.Location = new System.Drawing.Point(185, 0);
            this.labelGenre.Name = "labelGenre";
            this.labelGenre.Size = new System.Drawing.Size(94, 31);
            this.labelGenre.TabIndex = 12;
            this.labelGenre.Text = "Genre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Incorrect Guesses: ";
            // 
            // textBoxIncorrect
            // 
            this.textBoxIncorrect.Location = new System.Drawing.Point(22, 169);
            this.textBoxIncorrect.Multiline = true;
            this.textBoxIncorrect.Name = "textBoxIncorrect";
            this.textBoxIncorrect.ReadOnly = true;
            this.textBoxIncorrect.Size = new System.Drawing.Size(78, 79);
            this.textBoxIncorrect.TabIndex = 14;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(162, 204);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(157, 10);
            this.panel3.TabIndex = 11;
            // 
            // Gamefrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 305);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Gamefrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hangman";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelWord;
        private System.Windows.Forms.TextBox textBoxWord;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panelArmL;
        private System.Windows.Forms.Panel panelArmR;
        private System.Windows.Forms.Panel panelLegR;
        private System.Windows.Forms.Panel panelLegL;
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.Button btnGuess;
        private System.Windows.Forms.Label labelGuess;
        private System.Windows.Forms.TextBox textBoxGuess;
        private System.Windows.Forms.TextBox textBoxGuessWord;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxGenre;
        private System.Windows.Forms.Label labelGenre;
        private System.Windows.Forms.TextBox textBoxIncorrect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
    }
}