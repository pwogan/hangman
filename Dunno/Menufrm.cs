﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dunno
{
    public partial class Menufrm : Form
    {
        public Menufrm()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            this.Hide();
            Gamefrm newGame = new Gamefrm();
            newGame.Closed += (s, args) => this.Close();
            newGame.Show();
        }
    }
}
