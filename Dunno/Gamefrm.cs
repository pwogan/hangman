﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dunno
{
    public partial class Gamefrm : Form
    {
        public string word;
        public string guess;
        StringBuilder guessword = new StringBuilder("", 25);
        int lives;
        Panel[] hangman = new Panel[6];

        public Gamefrm()
        {
            InitializeComponent();
            hangman = new Panel[] { panelLegR, panelLegL, panelArmR, panelArmL, panelBody, panelHead };
            lives = 6;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            word = textBoxWord.Text.ToUpper();
            if (word.Length>0)
            {
                panel1.Visible = false;
                panel2.Visible = true;
            }
            
            for (int i = 0; i < word.Length; i++)
            {
                if(word[i].Equals(' '))
                {
                    guessword.Append("  ");
                }           
                else
                {
                    guessword.Append("_ ");
                }
                    
            }


            textBoxGuessWord.Text = guessword.ToString();
            string genre = textBoxGenre.Text;
            labelGenre.Text = genre;

        }

        private void btnGuess_Click(object sender, EventArgs e)
        {
            if(textBoxGuess.Text.Length == 1)
            {
                guess = textBoxGuess.Text.ToUpper();
                if(word.Contains(guess)==false)
                {
                    lives--;
                    ((Panel)hangman[lives]).Visible = true;
                    textBoxIncorrect.AppendText(guess + "\n");
                    if(lives==0)
                    {
                        MessageBox.Show("You lose\n\nThe word was - " + word.ToUpper());
                        this.Hide();
                        Menufrm menu = new Menufrm();
                        menu.Closed += (s, args) => this.Close();
                        menu.Show();
                    }
                }
                else
                {
                    for (int i = 0; i < word.Length; i++)
                    {
                        if(word[i].Equals(guess[0]))
                        {
                            guessword[i * 2] = guess[0];
                        }
                    }
                    textBoxGuessWord.Text = guessword.ToString();
                    if(textBoxGuessWord.Text.Contains("_") == false)
                    {
                        MessageBox.Show("You win!");
                        this.Hide();
                        Menufrm menu = new Menufrm();
                        menu.Closed += (s, args) => this.Close();
                        menu.Show();
                    }
                }
            }
            textBoxGuess.Text = "";
        }

        private void labelWord_Click(object sender, EventArgs e)
        {

        }
    }
}
